# Exercise - Add Dockerfile and test

## 1. Create Dockerfile

Add a Dockerfile to this project, put it in the root of the project (not in a sub-directory).

You can base it on a similar Dockerfile here:

https://bitbucket.org/fcallaly/trade-api/src/master/Dockerfile

You should change the base image to a newer java base image e.g. Java 11 - search on dockerhub for openjdk images

e.g. ```FROM openjdk:11-jre```

You will need to change the jar filename and path that is being copied into the Docker image

e.g. ```COPY target/northwind-shippers-api-0.0.1-SNAPSHOT.jar app.jar```

## 2. Test Dockerfile

Test the Dockerfile by building a new docker image: ( in the same directory as the Dockerfile)

**REMEMBER THE . ON THE END OF THE COMMAND BELOW!!**

```docker build -t shippers-demo:0.0.1 .```

Test that the image works:

```docker run -d --name shippers -e SPRING_PROFILE=h2 -p 8081:8080 shippers-demo:0.0.1```

Verify that you see the image is running:
```docker ps```

Verify that the application swagger ui is on port 8081

when you're done, stop the image:
```docker stop shippers```

## 3. Tag and Push to dockerhub

Sign up to dockerhub (do not use a work email!)

Login with the docker command line to dockerhub (it will ask for your user/pass):
```docker login```

Tag your image for dockerhub:
```docker tag shippers-demo:0.0.1 your-dockerhub-id/shippers-demo:0.0.1```

Now upload the image to dockerhub with a "push"
```docker push your-dockerhub-id/shippers-demo:0.0.1```

Verify that you can see the image on dockerhub

## 4. (Optional) Try running on a different machine

You can now run your docker image from any machine that has docker installed.

E.g. Ask someone else in your room if they can run your image - or you can try running theirs.

```
docker run -e SPRING_PROFILE=h2 -d your-dockerhub-id/shippers-demo:0.0.1
```

You should now have your image (and so your java application) available to run from anywhere that can run docker images.

