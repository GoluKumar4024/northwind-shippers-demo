/**
 * 
 */
let age=50;

function demoVariables()
{
	var name="dee";
	if (true)
	{
		console.log(name);
		document.getElementById("labelName").innerText=name;
		age++;
		console.log(age);
		document.querySelector("#d1 #labelAge").innerText=age;
		
	}
	//console.log(age);
		
}
//demoVariables();

function getShippers()
{
	const url="http://localhost:8080/api/shippers";
	fetch(url)//promise object to return data from Rest API
		.then(response => { return response.json();}) //resolve , data from resolve is passed to next then
		.then(shippers => {			
			if (shippers.length > 0) {
				 var temp = "";
				 shippers.forEach((itemData) => {
					 temp += "<tr>";
					 temp += "<td>" + itemData.name + "</td>";
					 temp += "<td>" + itemData.phone + "</td>";
					 temp+= "<td><button type='button' onclick='populateDialog(" + itemData.id + ")' class='btn btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal'>Edit</button></td></tr>";
					//button display a bootstrap modal dialog with id of exampleModal. 
					});
				 document.getElementById('tbodyShippers').innerHTML = temp; //populate the html element with the ID of  tbodyShippers with TR tags
				 }	
			})
		
}


function populateDialog(clicked_id)
	{
		
		//alert(clicked_id)	;	
		const url=`http://localhost:8080/api/shippers/${clicked_id}`;
	fetch(url)
		.then(response => { return response.json();})
		.then(shipper => {			
							 document.getElementById('modalName').value = shipper.name;
							 document.getElementById('modalPhone').value = shipper.phone;
							 document.getElementById('modalId').value = shipper.id;

			
			});

		
	}




function update()
{
	//read data from the form
	const data = { 	id: document.getElementById('modalId').value,
	 				name:document.getElementById('modalName').value,
		 			phone:document.getElementById('modalPhone').value  };
		 		
		 		
		//alert(data);
		//PUT request with body equal on data in JSON format
		fetch('http://localhost:8080/api/shippers', {
		  method: 'PUT',
		  headers: {
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify(data),//convert data object to a string
		})
		.then((response) => response.json())
		//Then with the data from the response in JSON...
		.then((data) => {
		  console.log('Success:', data);
			getShippers();
			

		})
		//Then with the error genereted...
		.catch((error) => {
		  console.error('Error:', error);
		});
			
}



function save()
{
	const data = { id: 0, 
					 name:document.getElementById('name').value, 
					phone:document.getElementById('phone').value  };
	alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8080/api/shippers', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  alert("saved" + data.name);
  getShippers();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
	
}